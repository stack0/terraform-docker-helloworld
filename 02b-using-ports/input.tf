
variable "image" {
  type = string
  description = "string, docker image"
}

variable "container_name" {
    type = string
    description = "string, container name"
}

variable "docker_host" {
  type = string
  description = "string, host configuration value"
  default = "unix:///var/run/docker.sock"
}

variable "container_ports" {
    type = list(object({
      internal=string,
      external=string
    }))
    description = "map(object), port object as defined by https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs/resources/container#nestedblock--ports"
    default = []
}