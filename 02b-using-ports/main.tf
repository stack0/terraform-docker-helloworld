terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
    # host = "unix:///var/run/docker.sock"

    # uncomment the following lines (and comment out the docker.sock) if you want to connect remotely
    # you may need to auto approve
    # host     = "ssh://myuser@1.2.3.4:22"
    host = var.docker_host
    ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null"]
}

# this resource establishes the docker image
resource "docker_image" "mydocker" {
  name = var.image
}

# this resource establishes the container
resource "docker_container" "mycontainer" {
  image = docker_image.mydocker.image_id
  name  = var.container_name

  dynamic "ports" {
    for_each = var.container_ports
    content {
      internal = ports.value.internal
      external = ports.value.external
    }
  }
}
