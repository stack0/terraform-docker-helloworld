
variable "image" {
  type = string
  description = "string, docker image"
}

variable "container_name" {
    type = string
    description = "string, container name"
}

variable "docker_host" {
  type = string
  description = "string, host configuration value"
  default = "unix:///var/run/docker.sock"
}

variable "num_containers" {
  type = number
  description = "number, number of containers to launch"
  default = 1
}

variable "port_assignment_list" {
  type = list(number)
  description = "list of port assignments, size should = num_containers"
  default = []
  validation {
    condition     = length(var.port_assignment_list) > 0
    error_message = "Port assignment not > 0"
  }
}