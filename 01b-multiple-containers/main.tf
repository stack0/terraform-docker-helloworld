terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
    # host = "unix:///var/run/docker.sock"

    # uncomment the following lines (and comment out the docker.sock) if you want to connect remotely
    # you may need to auto approve
    # host     = "ssh://myuser@1.2.3.4:22"
    host = var.docker_host
    ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null"]
}

# this resource establishes the docker image
resource "docker_image" "mydocker" {
  name = var.image
}

# this resource establishes the container
resource "docker_container" "mycontainer" {
  count = var.num_containers
  image = docker_image.mydocker.image_id
  name = "${format("%s%02d", var.container_name, count.index)}"
  ports {
    internal = 80
    external = var.port_assignment_list[count.index] # illustrates using count.index to indicate which port to use
  }

  # lifecycle {
  #   precondition {
  #     condition = length(var.port_assignment_list) == num_containers
  #   }
  # }
}

output "docker_containers" {
  value = keys({
    for index, d in docker_container.mycontainer.* : "${format("%s,%s,%s", index, d.name,d.id)}" => d
  })
}