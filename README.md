# terraform-docker-helloworld

This is a simple terraform template for docker

## 01{a,b,c}-multiple-containers

This part of the exercise shows how to use for_each and count, includes the following learning
* arithmetic
* string format
* for_each
* for
* terraform destroy -target
* output variables
* validation

## 02{a,b}-using-ports

This part of the exercise shows how to use maps, through the use of ports, includes the following learning
* dynamic blocks


