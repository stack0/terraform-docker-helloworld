
variable "image" {
  type = string
  description = "string, docker image"
}

variable "container_name" {
    type = string
    description = "string, container name"
}

variable "docker_host" {
  type = string
  description = "string, host configuration value"
  default = "unix:///var/run/docker.sock"
}

variable "containers_map" {
  type = map(string)
  description = "map(string), map where the key is the instance name and the value is the port to expose"
  default = {}
}