terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
    # host = "unix:///var/run/docker.sock"

    # uncomment the following lines (and comment out the docker.sock) if you want to connect remotely
    # you may need to auto approve
    # host     = "ssh://myuser@1.2.3.4:22"
    host = var.docker_host
    ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null"]
}

# this resource establishes the docker image
resource "docker_image" "mydocker" {
  name = var.image
}

# this resource establishes the container
resource "docker_container" "mycontainer" {
  for_each = var.containers_map
  image = docker_image.mydocker.image_id
  name = each.key
  ports {
    internal = 80
    external = each.value
  }
}

# output "docker_containers" {
#   value = keys({
#     for index, d in docker_container.mycontainer.* : "${format("%s,%s,%s", index, d.name,d.id)}" => d
#   })
# }

output "docker_containers" {
  value = keys({
    for k, d in docker_container.mycontainer : "${format("%s,%s", k, d.id)}" => d
  })
}
